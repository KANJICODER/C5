################################################################
## How to compile raylib:                                     ##
## https://www.youtube.com/watch?v=HPDLTQ4J_zQ                ##
################################################################
##                                                            ##
## -lraylib expands to: -libraylib                            ##
##  The ".a" is implied. So...                                ##
## -lraylib =====================> -libraylib.a               ##
##                                                            ##
## To avoid this type of expansion:                           ## 
##                                                            ## 
##     -L $FOLDER_PNGPAK_A                                    ## 
##     -l:PNG_ASSET_PACK_2022.a                               ## 
##                                                            ##
################################################################
################################################################
    C11_SOURCE_FILE="C5.C11"         ###########################

    FOLDER_RAYLIB_H="../C1/LIB/3_P/I"    #######################
    FOLDER_RAYLIB_A="../C1/LIB/3_P/L"    #######################

    FOLDER_RAYGUI_H="../C1/LIB/3_P/I"    #######################
    FOLDER_RAYGUI_A="../C1/LIB/3_P/L"    #######################

    FOLDER_PNGPAK_A="../PUTIN_OR_PUSSY_ASSETS/512_X_512/GEN"


    ## -L $FOLDER_PNGPAK_A     
    ## -l:PNG_ASSET_PACK_2022.a

################################################################




 ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "$C11_SOURCE_FILE"                                  \
        -o object_file.o                                       \
                                                               \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
    ############################################################
    echo "[ONTO_LIKING_PHASE]"
    ############################################################
    gcc -o  C5.EXE object_file.o                               \
                                                               \
            -I $FOLDER_RAYLIB_H                                \
            -I $FOLDER_RAYGUI_H                                \
                                                               \
            -L $FOLDER_RAYLIB_A                                \
            -L $FOLDER_RAYGUI_A                                \
                                                               \
            -lraylib                                           \
            -lopengl32                                         \
            -lgdi32                                            \
            -lwinmm  ###########################################
echo "[C2.SH:GOT_HERE:EXE_TIME]" 
    ############################################################
    rm             object_file.o    ####                    ####     
         ./C5.EXE                   ####                    ####
    rm     C5.EXE                   ####                    ####
                                    ####                    ####
    read -p "[ENTER_TO_EXIT]:"      ####                    ####
    ############################################################


